use std::fmt;

/// Represents errors that can occur while parsing iReal Pro URLs and
/// progressions.
#[derive(Debug)]
pub enum Error {
    InvalidUrl,
    MissingField(&'static str),
    InvalidNote,
    InvalidChordQuality,
    InvalidTimeSignature,
    InvalidStaffText,
    InvalidProgression,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::InvalidUrl => write!(f, "Invalid iReal Pro URL"),
            Error::InvalidNote => write!(f, "Invalid iReal Pro note"),
            Error::InvalidChordQuality => write!(f, "Invalid iReal Pro chord quality"),
            Error::InvalidProgression => write!(f, "Invalid chord progression"),
            Error::InvalidTimeSignature => write!(f, "Invalid time signature"),
            Error::InvalidStaffText => write!(f, "Invalid staff text"),
            Error::MissingField(field) => write!(f, "Missing field: {}", field),
        }
    }
}

impl std::error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;
