use std::{fmt, str::FromStr};

use crate::{Error, Result};

/// Represents the time signatures
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum TimeSignature {
    /// 4/4
    T44,
    /// 3/4
    T34,
    /// 2/4
    T24,
    /// 5/4
    T54,
    /// 6/4
    T64,
    /// 7/4
    T74,
    /// 2/2
    T22,
    /// 3/2
    T32,
    /// 5/8
    T58,
    /// 6/8
    T68,
    /// 7/8
    T78,
    /// 9/8
    T98,
    /// 12/8
    T12,
}

impl fmt::Display for TimeSignature {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TimeSignature::*;

        let s = match self {
            T44 => "T44",
            T34 => "T34",
            T24 => "T24",
            T54 => "T54",
            T64 => "T64",
            T74 => "T74",
            T22 => "T22",
            T32 => "T32",
            T58 => "T58",
            T68 => "T68",
            T78 => "T78",
            T98 => "T98",
            T12 => "T12",
        };

        write!(f, "{s}")
    }
}

impl FromStr for TimeSignature {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        use TimeSignature::*;

        let ts = match s {
            "T44" => T44,
            "T34" => T34,
            "T24" => T24,
            "T54" => T54,
            "T64" => T64,
            "T74" => T74,
            "T22" => T22,
            "T32" => T32,
            "T58" => T58,
            "T68" => T68,
            "T78" => T78,
            "T98" => T98,
            "T12" => T12,
            _ => return Err(Error::InvalidTimeSignature),
        };

        Ok(ts)
    }
}
